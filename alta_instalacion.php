<?php

/* 
 */
 include_once("Instalacion.php");
 include_once("config.php");
 //print_r($_REQUEST);
 

        function conectarMySQL($baseDatos){
            if ($baseDatos == ''){
              $dsn='mysql:host='.Config::$bdhostname ;  
            }else {
              $dsn='mysql:host='.Config::$bdhostname .';dbname='.$baseDatos;  
           }
         
           $username=Config::$bdusuario;
           $passwd=Config::$bdclave;
         
           try {
             $mySQL =new PDO($dsn, $username, $passwd) ;   
             $mySQL->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY,TRUE);
           
                return ($mySQL);
        } catch (PDOException $e) {
             echo 'Falló la conexión: ' . $e->getMessage(). "<br>";
}
          // $mySQL = new PDO($dsn, "root","root");  
        }
        function crearBaseDatos($conexion) {
             $consulta='CREATE DATABASE '.Config::$bdnombre;
             
        try {          
            $conexion->query($consulta);
           echo 'Creada BBDD : '.Config::$bdnombre. "<br>";
        } catch (PDOException $e) {
             echo 'Falló la conexión: ' . $e->getMessage(). "<br>";
        }   
        }
     // Crear tabla
     function crearTabla($conexion,$tabla, $claveP){
     if ($tabla=='provincia') {
        $consulta='CREATE TABLE '.$tabla .'( 
            codigo INTEGER NOT NULL,
            nominacion VARCHAR(50),
            superficie INTEGER,
            habitantes INTEGER,
            comunidad VARCHAR(50),
     PRIMARY KEY (' .$claveP  .') )'; 
     }
     
     if ($tabla=='poblacion') { 
        $consulta='CREATE TABLE '.$tabla .'( 
            codigo INTEGER NOT NULL,
            codigo_provincia INTEGER NOT NULL,
            nominacion VARCHAR(50),
            superficie INTEGER,
            habitantes INTEGER,
            gobierno VARCHAR(50),
     PRIMARY KEY (' .$claveP  .') )'; 
     }
       try {
              $conexion->query($consulta);
       echo 'CREADA LA TABLA: '.$tabla. "<br>";
        } catch (PDOException $e) {
             echo 'Falló la conexión: ' . $e->getMessage(). "<br>";
        } 
     }
     function claveAjena($conexion){
         $consulta='ALTER TABLE poblacion 
            ADD CONSTRAINT claveProvincia 
            FOREIGN KEY (codigo_provincia) 
            REFERENCES provincia (codigo) 
            ON DELETE NO ACTION 
            ON UPDATE NO ACTION ';
         try {
              $conexion->query($consulta);
                echo 'CREADA CAj: Población -> Provincia'. "<br>";
        } catch (PDOException $e) {
             echo 'Falló la conexión: ' . $e->getMessage(). "<br>";
        } 
         }
        // Crear tabla usuario
         function crearTablaUsuario($conexion){
             
           $consulta='CREATE TABLE usuario( 
            usuario VARCHAR(15) NOT NULL,
            clave  VARCHAR(15) NOT NULL,
            PRIMARY KEY (usuario) )'; 
     
       try {
              $conexion->query($consulta);
        // insertar el usuario admin/admin
              $consulta='INSERT INTO USUARIO (usuario,clave) 
              VALUES ("admin","admin")';
              $conexion->query($consulta);
              echo 'CREADA LA TABLA: usuario' ."<br>";
        } catch (PDOException $e) {
             echo 'Falló la conexión: ' . $e->getMessage(). "<br>";
         
         } }
               
        function ejecutar($obj_instalacion){  
     // Conectar
           
     $conexion=conectarMySQL(''); //sin base de datos
     crearBaseDatos($conexion); 
     $conexion=NULL;
     
     $baseDatos=$obj_instalacion->getBaseDatos();
     $tabla=$obj_instalacion->getTabla();
     $tabla2=$obj_instalacion->getTabla2();
     $claveP=$obj_instalacion->getClavePrimaria();
     $conexion=conectarMySQL($baseDatos); // con base de datos
     crearTabla($conexion,$tabla,$claveP); 
     crearTabla($conexion,$tabla2,$claveP);
     
     // Clave ajena
     claveAjena($conexion);
     // crear tabla usuario/login TEMA 8
     crearTablaUsuario($conexion);
     
     $conexion=NULL;
        }
?>