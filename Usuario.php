<?php

/* 
 clase Usuario
 */
class Usuario{
   //Atributos
    private $usuario;
    private $clave;
            
// constructor    
    public function __construct($usuario,$clave) {
        $this -> usuario = $usuario;
        $this -> clave = $clave;
             
    } 
    function getUsuario() {
        return $this->usuario;
    }

    function getClave() {
        return $this->clave;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setClave($clave) {
        $this->clave = $clave;
    }


    
}
?>