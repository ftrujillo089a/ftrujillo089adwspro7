<?php
error_reporting(E_ALL ^ E_NOTICE);
// Datos constantes.
    include_once ('config.php');
    include_once("Provincia.php"); 
    include_once("alta_instalacion.php");  
    include_once("control_funciones.php"); 
    sesion(); // función que comprueba si se ha introducido login
?>
 
<html>
    <head>
        <title>Poblaciones Españolas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/proyecto1.css">
    </head>
    <body>    
        
        <h1 id='titulo'><?=Config::$titulo?></h1>
    <div>ALTA POBLACIÓN</div>
        <form name="form1" method="post" action="alta_poblacion.php">
            <table> 
                <tr>
                    <td>Código:</td><td><input type="text" name="codigo"><br></td>
                </tr>
                <tr>
                    <td>Código Provincia:</td><td><select name="codigo_provincia"> 
        <!-- Incluir combo con las provincias -->  
     <?php
     // ------------------------------------------------ FICHERO
     if (Config::$modelo=='fichero') {
        $file = fopen("provincias.txt", "r");
        
            while (!feof($file)){
           $linea= fgets($file) ;
        //  dividir en variables (separación ;). Cada elemento del array tiene un dato
           $array_datos=  explode(';', $linea);      
           $obj_provincia=new Provincia($array_datos[0], $array_datos[1],"","","");
           echo'<option value="' .$array_datos[0] .'">' .$array_datos[1] .'</option>' ; 
                  }
            fclose ($file);
     }
 // ------------------------------------------------ BBDD
     if (Config::$modelo=='mysql') {
          // establecer conexión con la base de datos
         $conexion=conectarMySQL(Config::$bdnombre); // con base de datos
         $consulta='SELECT * FROM PROVINCIA';	
                   
          $datos=$conexion->query($consulta);
         foreach ($datos as $registro) {    
           $obj_provincia=new Provincia($registro[codigo],$registro[nominacion]);
           echo'<option value="' .$registro[codigo] .'">' .$registro[nominacion] .'</option>' ; 
         }        
         $conexion=NULL;  //cerrar

          }
     
      ?>
                        </select>          
                     
           </td>
                </tr>
            <tr>
                    <td>Nombre:</td><td><input type="text" name="nominacion"><br></td>
                </tr>
            <tr>
                    <td>Superficie:</td><td><input type="text" name="superficie"><br></td>
                </tr>
            <tr>
                    <td>Habitantes:</td><td><input type="text" name="habitantes"><br></td>
                </tr>
           <tr>
                    <td>Gobierno:</td><td><input type="text" name="gobierno"><br></td>
                </tr>
            <tr>
                    <td><input type="submit" value="Enviar"> </td>         
                    <td><input type="reset" value="Borrar"></td>
                </tr>            
                            
            </table>
        </form> 
    
     <?php volver_inicio(); ?>
    <div id="pie"><?=Config::$autor?> <?=Config::$fecha?> <?=Config::$empresa?> <?=Config::$curso?></div>    
    </body>
</html>

