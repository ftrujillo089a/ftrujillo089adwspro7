<?php
        
 include_once 'config.php'; 
 include_once("control_funciones.php"); 
    sesion(); // función que comprueba si se ha introducido login
  ?>
<!DOCTYPE html>
<!--
 Mantenimiento de provincias y sus poblaciones 
-->
     
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/proyecto1.css">
        <title>Provincias Españolas</title>
    </head>
    <body>
        <h1 id="titulo"><?=Config::$titulo?></h1><br><br>
        <h3>Error, debes introducir un usuario/contraseña correctos.</h3>
        <h3>Para altas ponerse en contacto con el administrador.</h3>
        <form name="form1" method="post" action="gestion_error.php">
        <input type="submit" value="Volver">        
        </form>        
      
     
        <div id="pie"><?=Config::$autor?> <?=Config::$fecha?> <?=Config::$empresa?> <?=Config::$curso?></div>    
    </body>                   
</html>
