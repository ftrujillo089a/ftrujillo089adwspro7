<?php
 session_start();       
 include_once 'config.php'; 
 include_once ('Usuario.php');
 include_once ('alta_usuario.php');
 
 // Control login
 if(isset($_REQUEST['usuario'])) {
        $usuar = htmlspecialchars(trim(strip_tags($_REQUEST['usuario'])));
        $clave = htmlspecialchars(trim(strip_tags($_REQUEST['password'])));
        $_SESSION['usuario'] = $_POST['usuario'];    // sesión
            // leer usuario para saber si existe
         $obj_usuario=new Usuario($usuar, $clave);
         $resul= leer($obj_usuario);
        
         if ($resul>0){ // existe, continuar  
            
           header("Location: menu_principal.php");                         
        }else {
           header("Location: form_error.php");  // Mostrar error
        }         
    } 
  ?>
<!DOCTYPE html>
<!--
 Mantenimiento de provincias y sus poblaciones 
-->
     
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/proyecto1.css">
        <title>Provincias Españolas</title>
    </head>
    <body>
        <h1 id="titulo"><?=Config::$titulo?></h1><br>
         
        <form name="form1" method="post" action="<?= $_SERVER["PHP_SELF"] ?>">
            <table> 
                <tr>
                    <td>Usuario:</td><td><input type="text" name="usuario"><br></td>
                </tr>
                 <tr>
                     <td>Password:</td><td><input type="password" name="password"><br></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Enviar"> </td>         
                    <td><input type="reset" value="Borrar"></td>
                </tr> 
            </table>
        </form>        
      
        <div id="pie"><?=Config::$autor?> <?=Config::$fecha?> <?=Config::$empresa?> <?=Config::$curso?></div>    
    </body>                   
</html>
